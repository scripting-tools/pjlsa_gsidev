__version__ = "1.1.0"

__gradle_deps__ = [
    {
        "groupId": "de.gsi.lsa.core",
        "product": "lsa-client-gsi",
        "version": "20.0.0-SNAPSHOT",
        "repository": 'maven { url "https://artifacts.acc.gsi.de/repository/default/" }',
    },
    {
        "groupId": "de.gsi.aco.app",
        "product": "acoapp-third-party-bom",
        "version": "20.0.0-RC-INT-SNAPSHOT",
        "type": "enforcedPlatform",
        "repository": 'maven { url "https://artifacts.acc.gsi.de/repository/default/" }',
    },
]

__all__ = ["LSAClientGSI"]

from .lsa_client import LSAClientGSI
