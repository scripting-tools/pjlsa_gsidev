import pjlsa


class LSAClientGSI(pjlsa.BaseLSAClient):
    def __init__(self) -> None:
        """Connect to the development LSA server at GSI."""
        cfg_url = (
            "http://config.development.lb.a.k8s.acc.gsi.de/application/profile/dev/"
        )
        super().__init__(
            "gsi-dev",
            {
                "csco.default.property.config.url": cfg_url,
                "log4j.configurationFile": "log4j2-dev.xml",
            },
        )
