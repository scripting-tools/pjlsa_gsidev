#!/usr/bin/env python3

"""Prints all available GSI patterns using the LSA context service."""

import pjlsa_gsidev

with pjlsa_gsidev.LSAClientGSI().java_api():
    from cern.lsa.client import ContextService, ServiceLocator

context_service = ServiceLocator.getService(ContextService)

for pattern in context_service.findPatterns():
    print(pattern)
